package Model;

public class Calculadora {

    /*
    Programe una clase calculadora que permita realizar las siguientes operaciones: sumar, restar, multiplicar y dividir.
    Que muestre un mensaje de error si el divisor es 0. El valor debe mostrar con decimales

    IEEE 754 standards
     */

    static double calculadora(double valor1, double valor2, String operacion){
        double resultado = 0;

        if(valor2 == 0 && operacion.equals("/")){
           System.out.println("Error: El valor no puede ser 0");
           return 0;
        }

        switch (operacion){
            case "+":
                resultado = valor1+valor2;
                break;
            case "-":
                resultado = valor1-valor2;
                break;
            case "*":
                resultado = valor1*valor2;
                break;
            case "/":
                resultado = valor1/valor2;
                break;
        }
        return resultado;
    }

    static double calculadora2(double valor1, double valor2, String operacion){
        double resultado = 0;

       try {
           switch (operacion){
               case "+":
                   resultado = valor1+valor2;
                   break;
               case "-":
                   resultado = valor1-valor2;
                   break;
               case "*":
                   resultado = valor1*valor2;
                   break;
               case "/":
                   resultado = valor1/valor2;
                   if(resultado == Double.POSITIVE_INFINITY || resultado == Double.NEGATIVE_INFINITY || valor2 == 0){
                      throw new ArithmeticException();
                   }
                   break;
           }
       }catch (ArithmeticException e){
           System.out.println("No se puede dividr un número entre 0");
       }

        return resultado;
    }

    public static void main(String[] args) {
        System.out.println(calculadora2(2,0,"/"));
    }
}
