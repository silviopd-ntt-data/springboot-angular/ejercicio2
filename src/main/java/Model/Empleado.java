package Model;

public class Empleado {

    /*
    Cree una clase Empleado que modele la información que una empresa mantiene sobre cada empleado:
    nombre y apellidos, DNI, edad, sueldo base, pago por hora extra, horas extra realizadas en el mes,
    sexo, casado o no, número de hijos. Deberá tener los siguientes métodos:
        * Calculo de sueldo Bruto
        * Mostrar detalle del empleado (Nombre completo, DNI, ingreso bruto, sueldo base,
        ingreso total por horas extras, si es mayor de edad, estado civil y nro de hijos)
     */

    static String nombre, apellidos,  dni,  sexo,  casado;
    static int edad, nroHijos;
    static double sueldoBase, pagoHoraExtra, horasExtraMes;

    static double sueldoBruto(double sueldoBase, double pagoHoraExtra, double horasExtraMes){
        return sueldoBase+pagoHoraExtra*horasExtraMes;
    }

    public static void main(String[] args) {
        nombre = "Silvio";
        apellidos= "Peña Diaz";
        dni = "46705014";
        sexo = "Masculino";
        casado = "No";
        edad = 30;
        nroHijos = 0;
        sueldoBase = 2000;
        pagoHoraExtra = 10;
        horasExtraMes = 10;

        double sueldoBruto = sueldoBruto(sueldoBase,pagoHoraExtra,horasExtraMes);
        double ingresoHorasExtra = pagoHoraExtra*horasExtraMes;

        System.out.println("Nombre y Apellidos: "+nombre+" "+apellidos);
        System.out.println("Dni: "+dni);
        System.out.println("Sueldo bruto: "+sueldoBruto);
        System.out.println("Sueldo base: "+sueldoBase);
        System.out.println("Ingreso total por horas extras: "+ingresoHorasExtra);
        System.out.println( edad >= 18 ? "Mayor de edad" : "Menor de edad");
        System.out.println("Estado civil: "+ (casado == "No" ? "Soltero" : "Casado"));
        System.out.println("Nro de hijos: "+nroHijos);
    }
}
